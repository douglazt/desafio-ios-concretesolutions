//
//  MockNetworkClient.swift
//  Github JavaPopTests
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import Foundation
@testable import Github_JavaPop

class MockNetworkClient: NetworkService {
    
    lazy var items: [Repository] = {
        let repos:[Repository] = [
            Repository(JSONString: "{\"id\": \(1), \"name\": \"Repo1\"}")!
        ]
        return repos
    }()
    
    lazy var pullRequests: [PullRequest] = {
        let pulls: [PullRequest] = [PullRequest(JSONString: "{\"id\": \(1), \"title\": \"pull1\"}")!]
        return pulls
    }()
    
    func getRepositories(forLanguage language: String, page: Int, resultsPerPage: Int, successBlock: @escaping ([Repository]) -> Void, failureBlock: @escaping (Error) -> Void) {
        successBlock(items)
    }
      
    func getPullRequests(forRepository: Repository, page: Int, resultsPerPage: Int, successBlock _success: @escaping ([PullRequest]) -> Void, failureBlock _failure: @escaping (Error) -> Void) {
        
        _success(pullRequests)
    }
    
}
