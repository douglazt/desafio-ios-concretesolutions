//
//  MenuTests.swift
//  Github JavaPopTests
//
//  Created by Douglas da silva santos on 17/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import XCTest
import MMDrawerController
@testable import Github_JavaPop

class MenuTests: XCTestCase {
    
    var drawer: MMDrawerController!
    var delegate: AppDelegate!
    
    override func setUp() {
        super.setUp()
        delegate = AppDelegate()
        delegate.window = UIWindow()
        delegate.configureDrawer()
        drawer = delegate.window?.rootViewController as! MMDrawerController
    }
    
    override func tearDown() {
        //menuVC = nil
        drawer = nil
        delegate = nil
        super.tearDown()
    }
    
    func testMenuItemsConfiguration() {
        //given
        _ = ((drawer.leftDrawerViewController as? UINavigationController)?.topViewController as? MenuTableViewController)?.view
        //when
        let menuVC = (drawer.leftDrawerViewController as? UINavigationController)?.topViewController as? MenuTableViewController
        let items = menuVC!.menuItems.count
        //then
        XCTAssert(items > 0)
    }
}
