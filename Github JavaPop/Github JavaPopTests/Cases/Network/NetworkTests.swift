//
//  NetworkTests.swift
//  Github JavaPopTests
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import XCTest
@testable import Github_JavaPop

class NetworkTests: XCTestCase {
    
    var networkClient: NetworkClient!
    
    override func setUp() {
        super.setUp()
        networkClient = NetworkClient.shared
    }
    
    override func tearDown() {
        super.tearDown()
        networkClient = nil
    }
    
    func testLoadRepositories() {
        let expect = expectation(description: "load repositories")
        networkClient.getRepositories(forLanguage: "Java", page: 1, resultsPerPage: 10, successBlock: { (repositories) in
            XCTAssertNotNil(repositories)
            expect.fulfill()
        }) { (error) in
            XCTFail("could not download repositories: \(error.localizedDescription)")
        }
        
        wait(for: [expect], timeout: 5.0)
    }
    
    func testLoadPullRequests(){
        //given
        let owner: RepositoryOwner = RepositoryOwner(JSONString: "{\"login\":\"JetBrains\"}")!
        var repository: Repository = Repository(JSONString: "{\"name\":\"kotlin\"}")!
        repository.owner = owner
        
        //when
        let expect = expectation(description: "load pull requests")
        networkClient.getPullRequests(forRepository: repository, page: 1, resultsPerPage: 10, successBlock: { (pullRequests) in
            XCTAssertNotNil(pullRequests)
            expect.fulfill()
        }) { (error) in
            XCTFail()
        }
        
        wait(for: [expect], timeout: 5.0)
    }
}



