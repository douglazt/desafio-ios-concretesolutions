//
//  RepoTests.swift
//  Github JavaPopTests
//
//  Created by Douglas da silva santos on 17/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import Github_JavaPop

class RepoTests: XCTestCase {
    
    var reposVC: RepositoriesTableViewController!
    var pullRequestsVC: PullRequestsTableViewController!
    var mockNetworkClient: MockNetworkClient!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        reposVC = storyboard.instantiateViewController(withIdentifier: "RepoViewController") as! RepositoriesTableViewController
        pullRequestsVC = storyboard.instantiateViewController(withIdentifier: "PullRequestsViewController") as! PullRequestsTableViewController
        
        mockNetworkClient = MockNetworkClient()
        reposVC.networkClient = mockNetworkClient
        pullRequestsVC.networkClient = mockNetworkClient
    }
    
    override func tearDown() {
        super.tearDown()
        reposVC = nil
        pullRequestsVC = nil
        mockNetworkClient = nil
    }
    
    func testThatCanLoadRepositories() {
        //when
        _ = reposVC.view
        //then
        XCTAssertTrue(reposVC.items.count == 1, "items count: \(reposVC.items.count) not equal: 1")
    }
    
    func testThatRepositoriesTableViewHaveLoadedItems(){
        //when
        _ = reposVC.view
        //then
        let rowsCount = reposVC.tableView.numberOfRows(inSection: 0)
        let firstCell = reposVC.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! RepoCell
        XCTAssertTrue(rowsCount == 1, "rows count: \(rowsCount) not equal: 1")
        XCTAssertEqual(firstCell.repositoryName.text, "Repo1")
    }
    
    func testThatCanAddMoreRepositories() {
        //given
        _ = reposVC.view
        let mockedItem2 = Repository(JSONString: "{\"id\": \(2), \"name\": \"Repo2\"}")!
        let mockedItem3 = Repository(JSONString: "{\"id\": \(3), \"name\": \"Repo3\"}")!
        //when
        reposVC.appendItemsToDataSource(repositories: [mockedItem2, mockedItem3])
        //then
        XCTAssertEqual(reposVC.items.count, 3)
    }
    
    func testThatCannotAddMoreRepeatedRepositories() {
        //when
        _ = reposVC.view
        //then
        reposVC.appendItemsToDataSource(repositories: mockNetworkClient.items)
        reposVC.appendItemsToDataSource(repositories: mockNetworkClient.items)
        XCTAssertEqual(reposVC.items.count, 1)
    }
    
    func testThatCanLoadPullRequests() {
        //given
        setSelectedRepoForPullRequestsTableViewController()
        //when
        _ = pullRequestsVC.view
        //then
        XCTAssertTrue(pullRequestsVC.items.count == 1, "items count: \(pullRequestsVC.items.count) not equal: 1")
    }

    func testThatPullRequestsTableViewHaveLoadedItems(){
        //given
        setSelectedRepoForPullRequestsTableViewController()
        //when
        _ = pullRequestsVC.view
        //then
        let rowsCount = pullRequestsVC.tableView.numberOfRows(inSection: 0)
        let firstCell = pullRequestsVC.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! PullRequestCell
        XCTAssertTrue(rowsCount == 1, "rows count: \(rowsCount) not equal: 1")
        XCTAssertEqual(firstCell.pullRequestTitle.text, "pull1")
    }

    func testThatCanAddMorePullRequests() {
        //given
        setSelectedRepoForPullRequestsTableViewController()
        let mockedItem2 = PullRequest(JSONString: "{\"id\": \(2), \"title\": \"pull2\"}")!
        let mockedItem3 = PullRequest(JSONString: "{\"id\": \(3), \"title\": \"pull3\"}")!
        //when
        _ = pullRequestsVC.view
        pullRequestsVC.appendItemsToDataSource(pullRequests: [mockedItem2, mockedItem3])
        //then
        XCTAssertEqual(pullRequestsVC.items.count, 3)
    }

    func testThatCannotAddMoreRepeatedPullRequests() {
        //given
        setSelectedRepoForPullRequestsTableViewController()
        //when
        _ = pullRequestsVC.view
        //then
        pullRequestsVC.appendItemsToDataSource(pullRequests: mockNetworkClient.pullRequests)
        pullRequestsVC.appendItemsToDataSource(pullRequests: mockNetworkClient.pullRequests)
        XCTAssertEqual(pullRequestsVC.items.count, 1)
    }
    
    //Helpers
    func setSelectedRepoForPullRequestsTableViewController() {
        let owner: RepositoryOwner = RepositoryOwner(JSONString: "{\"login\":\"JetBrains\"}")!
        var repository: Repository = Repository(JSONString: "{\"name\":\"kotlin\"}")!
        repository.owner = owner
        pullRequestsVC.selectedRepository = repository
    }
}

