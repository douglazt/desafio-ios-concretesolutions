//
//  AppDelegate.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 16/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import UIKit
import MMDrawerController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        configureDrawer()
        return true
    }

    internal func configureDrawer() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let menuController = storyboard.instantiateViewController(withIdentifier: "MenuNavigationController") as! UINavigationController
        let centerController = storyboard.instantiateViewController(withIdentifier: "RepoNavigationViewController") as! UINavigationController
        let drawer = MMDrawerController(center: centerController, leftDrawerViewController: menuController)
        
        drawer?.openDrawerGestureModeMask = .all
        drawer?.closeDrawerGestureModeMask = .all
        
        window?.rootViewController = drawer
    }

}

