//
//  AboutViewController.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 16/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import UIKit
import MMDrawerController

class AboutViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("About", comment: "")
        configureMenuButton()
        configureLabels()
    }
    
    internal func configureLabels() {
        titleLabel.text = NSLocalizedString("GitHub API query application", comment: "")
        subtitleLabel.text = NSLocalizedString("Test for iOS Developer at Concrete Solutions", comment: "")
    }
    
    internal func configureMenuButton(){
        navigationController?.navigationBar.tintColor = .white
        navigationItem.leftBarButtonItem = MMDrawerBarButtonItem(target: self, action: #selector(openDrawer))
    }
    
    @objc func openDrawer(){
        if mm_drawerController.openSide == .none {
            mm_drawerController.open(.left, animated: true, completion: nil)
        } else {
            mm_drawerController.closeDrawer(animated: true, completion: nil)
        }
    }
    
}
