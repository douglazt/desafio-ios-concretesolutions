//
//  RepositoryViewModel.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class RepositoryViewModel {
    
    var model: Repository!
    init(repository: Repository) {
        model = repository
    }
    
    func configureCell(cell: RepoCell) {
        cell.repositoryName.text = model.name
        cell.repositoryDescription.text = model.repoDescription
        cell.repositoryForksCount.text = "\(model.forks_count ?? 0)"
        cell.repositoryStarsCount.text = "\(model.stargazers_count ?? 0)"
        cell.repositoryOwnerName.text = model.owner?.login
        configureOwnerImage(imageView: cell.repositoryOwnerImage)
    }
    
    func configureOwnerImage(imageView: UIImageView){
        guard let urlString = model.owner?.avatar_url else { return }
        guard let url = URL(string: urlString) else { return }
        
        imageView.af_setImage(withURL: url)
    }
    
}
