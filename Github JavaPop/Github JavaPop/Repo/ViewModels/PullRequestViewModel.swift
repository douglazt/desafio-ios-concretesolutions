//
//  PullRequestViewModel.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class PullRequestViewModel {
    
    var model: PullRequest!
    init(pullRequest: PullRequest) {
        model = pullRequest
    }
    
    func configureCell(cell: PullRequestCell) {
        cell.pullRequestTitle.text = model.title
        cell.pullRequestBody.text = model.body
        cell.pullRequestDate.text = formatedDate()
        cell.pullRequestUserName.text = model.user?.login
        configureAssigneImage(imageView: cell.pullRequestUserImage)
    }
    
    internal func formatedDate() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.locale = Locale.current
        if let updateDate = model.updated_at {
            return formatter.string(from: updateDate)
        }
        
        if let createDate = model.created_at {
            return formatter.string(from: createDate)
        }
        
        return ""
    }
    
    internal func configureAssigneImage(imageView: UIImageView){
        guard let urlString = model.user?.avatar_url else { return }
        guard let url = URL(string: urlString) else { return }
        
        imageView.af_setImage(withURL: url)
    }
    
}
