//
//  PullRequest.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import Foundation
import ObjectMapper

public struct PullRequest: Mappable {
    
    var id: Int?
    var html_url: String?
    var number: Int?
    var state: String?
    var title: String?
    var body: String?
    var created_at: Date?
    var updated_at: Date?
    var closed_at: Date?
    var merged_at: Date?
    var user: PullRequestUser?
    
    public mutating func mapping(map: Map) {
        id <- map["id"]
        html_url <- map["html_url"]
        number <- map["number"]
        state <- map["state"]
        title <- map["title"]
        body <- map["body"]
        user <- map["user"]
        created_at <- (map["created_at"], DateFormatTransform())
        updated_at <- (map["updated_at"], DateFormatTransform())
        closed_at <- (map["closed_at"], DateFormatTransform())
        merged_at <- (map["merged_at"], DateFormatTransform())
    }
    
    public init?(map: Map) {
        
    }
    
}

public class DateFormatTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = String
    
    var dateFormatter = DateFormatter(withFormat: "yyyy-MM-dd'T'HH:mm:ssZZZ", locale: Locale.current.identifier)
    
    public func transformFromJSON(_ value: Any?) -> Date? {
        if let dateString = value as? String {
            return self.dateFormatter.date(from: dateString)
        }
        return nil
    }
    
    public func transformToJSON(_ value: Date?) -> String? {
        if let date = value {
            return self.dateFormatter.string(from: date)
        }
        return nil
    }
    
}

