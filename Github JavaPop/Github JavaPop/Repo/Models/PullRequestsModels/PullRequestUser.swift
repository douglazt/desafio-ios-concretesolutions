//
//  PullRequestAssignee.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import Foundation
import ObjectMapper

struct PullRequestUser: Mappable {
    
    var login: String?
    var id: Int?
    var avatar_url: String?
    var url: String?
    var type: String?
    
    mutating func mapping(map: Map) {
        login <- map["login"]
        id <- map["id"]
        avatar_url <- map["avatar_url"]
        url <- map["url"]
        type <- map["type"]
    }
    
    init?(map: Map) {
        
    }
}

