//
//  Repositories.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import Foundation
import ObjectMapper

struct Repositories: Mappable {
    
    var total_count: Int?
    var incomplete_results: Bool?
    var items: [Repository]?
    
    mutating func mapping(map: Map) {
        total_count <- map["total_count"]
        incomplete_results <- map["incomplete_results"]
        items <- map["items"]
    }
    
    init?(map: Map) {
        
    }
}
