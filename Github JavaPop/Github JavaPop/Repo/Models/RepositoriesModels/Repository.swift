//
//  Repository.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import Foundation
import ObjectMapper

public struct Repository: Mappable {
    
    var id: Int?
    var name: String?
    var full_name: String?
    var repoDescription: String?
    var html_url: String?
    var created_at: String?
    var updated_at: String?
    var size: Int? //KBytes
    var stargazers_count: Int?
    var watchers_count: Int?
    var language: String?
    var forks_count: Int?
    var owner: RepositoryOwner?
    
    public mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        full_name <- map["full_name"]
        repoDescription <- map["description"]
        html_url <- map["html_url"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        size <- map["size"]
        stargazers_count <- map["stargazers_count"]
        watchers_count <- map["watchers_count"]
        language <- map["language"]
        forks_count <- map["forks_count"]
        owner <- map["owner"]
    }
    
    public init?(map: Map) {
        
    }
    
}
