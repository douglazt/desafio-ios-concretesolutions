//
//  PullRequestsTableViewController.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import UIKit

class PullRequestsTableViewController: BaseTableViewController {

    //MARK: - Properties
    internal var items: [PullRequest] = []
    private let pullRequestCellIdentifier = "PullRequestCell"
    private let pullRequestTitle = NSLocalizedString("Pull Requests", comment: "")
    
    var selectedRepository: Repository?

    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = selectedRepository?.name
        configureRefreshControl()
        loadPullRequests()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        tableView.reloadData()
    }
    
    //MARK: - Configurations
    internal func configureRefreshControl() {
        tableRefreshControl.addTarget(self, action: #selector(loadPullRequests), for: .valueChanged)
        tableView.addSubview(tableRefreshControl)
    }
    
    //MARK: - Network
    @objc internal func loadPullRequests() {
        guard let repository = selectedRepository else { return }
        tableRefreshControl.beginRefreshing()
        networkClient.getPullRequests(forRepository: repository, page: currentPage, resultsPerPage: pagesPerRequest, successBlock: { [weak self] pullRequests in
            guard let strongSelf = self else { return }
            strongSelf.hasLoaded = true
            strongSelf.appendItemsToDataSource(pullRequests: pullRequests)
            strongSelf.tableView.reloadData()
            strongSelf.tableRefreshControl.endRefreshing()
        }) { [weak self] error in
            guard let strongSelf = self else { return }
            strongSelf.hasLoaded = true
            strongSelf.tableRefreshControl.endRefreshing()
            strongSelf.setTableViewBackground(error: true)
        }
        
    }
    
    func appendItemsToDataSource(pullRequests: [PullRequest]){
        if pullRequests.count == 0 {
            stopLoading = true
        }
        
        for pull in pullRequests {
            let contains = items.contains(where: { (pullRequest) -> Bool in
                return pull.id == pullRequest.id
            })
            
            if !contains {
                items.append(pull)
            }
        }
    }
    
    // MARK: - Table view data source
    private func tableViewSectionsConfiguration() -> Int {
        if !hasLoaded {
            tableView.backgroundView = nil
            return 0
        }
        
        if items.count == 0 {
            setTableViewBackground()
            return 0
        }
        
        tableView.backgroundView = nil
        return 1
    }
        
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewSectionsConfiguration()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: pullRequestCellIdentifier, for: indexPath) as! PullRequestCell
        let viewModel = PullRequestViewModel(pullRequest: items[indexPath.row])
        cell.configureWith(viewModel: viewModel)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    //MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 44))
        view.backgroundColor = .white
        
        let label = UILabel(frame: view.frame)
        label.text = pullRequestTitle
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = .darkGray
        label.textAlignment = .center
        view.addSubview(label)
        
        let bottomLine =  UIView(frame: CGRect(x: 0, y: view.frame.maxY - 1.0, width: view.frame.width, height: 2.0))
        bottomLine.backgroundColor = .lightGray
        view.addSubview(bottomLine)
        
        return view
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = items.count - 1
        if indexPath.row == lastItem {
            currentPage += 1
            if !stopLoading {
                loadPullRequests()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let pullRequest = items[indexPath.row]
        guard let urlString = pullRequest.html_url else { return }
        guard let url = URL(string: urlString) else { return }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
}
