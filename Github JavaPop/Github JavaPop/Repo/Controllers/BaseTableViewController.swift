//
//  BaseTableViewController.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 19/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import UIKit
import ObjectMapper

class BaseTableViewController: UITableViewController {

    //MARK: - Properties
    internal var networkClient: NetworkService! = NetworkClient.shared
    internal let tableRefreshControl = UIRefreshControl()
    internal var hasLoaded: Bool = false
    internal var currentPage: Int = 1
    internal var stopLoading: Bool = false
    internal var pagesPerRequest: Int = 30
    
    internal func configureTableView() {
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    internal func setTableViewBackground(error: Bool = false) {
        let noDataLabel = UILabel(frame: tableView.bounds)
        if error {
            noDataLabel.text = NSLocalizedString("Could not load data, pull down to refresh", comment: "")
        } else {
            noDataLabel.text = NSLocalizedString("There's no pull requests for the repository", comment: "")
        }
        noDataLabel.textAlignment = .center
        noDataLabel.numberOfLines = 0
        noDataLabel.font = UIFont.boldSystemFont(ofSize: 17)
        noDataLabel.textColor = .black
        tableView.separatorStyle = .none
        tableView.backgroundView = noDataLabel
    }
    
}
