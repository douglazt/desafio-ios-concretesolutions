//
//  RepositoriesTableViewController.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 16/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import UIKit
import MMDrawerController

class RepositoriesTableViewController: BaseTableViewController {

    //MARK: - Properties
    internal var items: [Repository] = []    
    private let repoCellIdentifier = "RepoCell"
    private let pullRequestSegueIdentifier = "PullRequestsSegue"
    private let languageIdentifer = "Java"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavBar()
        configureTableView()
        configureRefreshControl()
        configureMenuButton()
        loadRepositories()
    }
    
    //MARK: - Configurations
    
    internal func configureNavBar(){
        navigationItem.backBarButtonItem?.setTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: -60), for: .default)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    private func configureRefreshControl() {
        tableRefreshControl.addTarget(self, action: #selector(loadRepositories), for: .valueChanged)
        tableView.addSubview(tableRefreshControl)
    }
    
    private func configureMenuButton(){
        navigationController?.navigationBar.tintColor = .white
        navigationItem.leftBarButtonItem = MMDrawerBarButtonItem(target: self, action: #selector(openDrawer))
    }
    
    @objc func openDrawer(){
        if mm_drawerController.openSide == .none {
            mm_drawerController.open(.left, animated: true, completion: nil)
        } else {
            mm_drawerController.closeDrawer(animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? PullRequestsTableViewController {
            guard let repo = sender as? Repository else {
                return
            }
            viewController.selectedRepository = repo
        }
    }
    
    //MARK: - Network
    @objc internal func loadRepositories() {
        tableRefreshControl.beginRefreshing()
        networkClient.getRepositories(forLanguage: languageIdentifer, page: currentPage, resultsPerPage: pagesPerRequest, successBlock: { [weak self] repositories in
            guard let strongSelf = self else { return }
            strongSelf.hasLoaded = true
            strongSelf.appendItemsToDataSource(repositories: repositories)
            strongSelf.tableView.reloadData()
            strongSelf.tableRefreshControl.endRefreshing()
        }) { [weak self] error in
            guard let strongSelf = self else { return }
            strongSelf.hasLoaded = true
            strongSelf.tableRefreshControl.endRefreshing()
            strongSelf.setTableViewBackground(error: true)
        }
    }
    
    func appendItemsToDataSource(repositories: [Repository]){
        if repositories.count == 0 {
            stopLoading = true
        }
        
        for repo in repositories {
            let contains = items.contains(where: { (savedRepo) -> Bool in
                return repo.id == savedRepo.id
            })
            
            if !contains {
                items.append(repo)
            }
        }
    }
    
    //MARK: - Table view data source
    private func tableViewSectionsConfiguration() -> Int {
        if !hasLoaded {
            tableView.backgroundView = nil
            return 0
        }

        if items.count == 0 {
            setTableViewBackground()
            return 0
        }

        tableView.backgroundView = nil
        return 1
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewSectionsConfiguration()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: repoCellIdentifier, for: indexPath) as! RepoCell
        let repoViewModel = RepositoryViewModel(repository: items[indexPath.row])
        cell.configureCellWith(repositoryViewModel: repoViewModel)
        return cell
    }
 
    //MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let repo = items[indexPath.row]
        performSegue(withIdentifier: pullRequestSegueIdentifier, sender: repo)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = items.count - 1
        if indexPath.row == lastItem {
            currentPage += 1
            if !stopLoading {
                loadRepositories()
            }
            
        }
    }

}
