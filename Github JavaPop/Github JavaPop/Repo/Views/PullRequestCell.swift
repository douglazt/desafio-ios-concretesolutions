//
//  PullRequestCell.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import UIKit

class PullRequestCell: UITableViewCell {
    
    @IBOutlet weak var pullRequestTitle: UILabel!
    @IBOutlet weak var pullRequestBody: UILabel!
    @IBOutlet weak var pullRequestDate: UILabel!
    @IBOutlet weak var pullRequestUserName: UILabel!
    @IBOutlet weak var pullRequestUserImage: UIImageView!
    
    
    func configureWith(viewModel: PullRequestViewModel) {
        viewModel.configureCell(cell: self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        pullRequestTitle.text = nil
        pullRequestBody.text = nil
        pullRequestDate.text = nil
        pullRequestUserName.text = nil
        pullRequestUserImage.image = nil
        
    }
}
