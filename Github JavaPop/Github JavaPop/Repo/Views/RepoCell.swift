//
//  RepoCell.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import UIKit

class RepoCell: UITableViewCell {
    
    @IBOutlet weak var repositoryName: UILabel!
    @IBOutlet weak var repositoryDescription: UILabel!
    @IBOutlet weak var repositoryForksCount: UILabel!
    @IBOutlet weak var repositoryStarsCount: UILabel!
    @IBOutlet weak var repositoryOwnerName: UILabel!
    @IBOutlet weak var repositoryOwnerImage: UIImageView!
    
    func configureCellWith(repositoryViewModel: RepositoryViewModel) {
        repositoryViewModel.configureCell(cell: self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        repositoryName.text = nil
        repositoryDescription.text = nil
        repositoryForksCount.text = nil
        repositoryStarsCount.text = nil
        repositoryOwnerName.text = nil
        repositoryOwnerImage.image = nil
    }
    
}
