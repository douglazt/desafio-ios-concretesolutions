//
//  NetworkService.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import Foundation

public protocol NetworkService {
    
    func getRepositories(forLanguage language: String,
                        page: Int,
                        resultsPerPage: Int,
                        successBlock _success: @escaping ([Repository]) -> Void,
                        failureBlock _failure: @escaping (Error) -> Void)
    
    func getPullRequests(forRepository: Repository,
                         page: Int,
                         resultsPerPage: Int,
                         successBlock _success: @escaping ([PullRequest]) -> Void,
                         failureBlock _failure: @escaping (Error) -> Void)
}
