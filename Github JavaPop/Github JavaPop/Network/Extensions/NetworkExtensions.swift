//
//  NetworkExtensions.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import Foundation

extension Int {
    public var isSuccessHTTPCode: Bool {
        return 200 <= self && self < 300
    }
}
