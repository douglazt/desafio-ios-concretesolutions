//
//  NetworkClient.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 18/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

final class NetworkClient: NetworkService {
 
    private let baseURL: URL
    private let clientID: String = "Iv1.0b11e8c0730b2d3a"
    private let clientSecret: String = "4672b210958b0586713109a32b796d7923e0849b"
    
    public static let shared: NetworkClient = {
        let urlString = "https://api.github.com/"
        let url = URL(string: urlString)!
        return NetworkClient(baseURL: url)
    }()
    
    private init(baseURL: URL) {
        self.baseURL = baseURL
    }
    
    func getRepositories(forLanguage language: String,
                                page: Int,
                                resultsPerPage: Int,
                                successBlock _success: @escaping (_ repos:[Repository]) -> Void,
                                failureBlock _failure: @escaping (_ error: Error) -> Void) {
        
        let success: ([Repository]) -> Void = { repos in
            DispatchQueue.main.async { _success(repos) }
        }
        let failure: (Error) -> Void = { error in
            DispatchQueue.main.async { _failure(error) }
        }
        
        let params: Parameters = [
            "q": "language:\(language)",
            "sort":"stars",
            "page": page,
            "per_page": resultsPerPage,
            "client_id": clientID,
            "client_secret": clientSecret
            ]
        
        let url = baseURL.appendingPathComponent("search/repositories")
        
        Alamofire.request(url, method: .get, parameters: params, headers: nil).responseObject { (response: DataResponse<Repositories>) in
            
            //verify for errors
            guard let httpResponse = response.response,
                httpResponse.statusCode.isSuccessHTTPCode,
                let repositories = response.result.value,
                let items = repositories.items else {
                    if let error = response.error {
                        failure(error)
                    } else {
                        //generic error
                        let genError = NSError(domain: "Github JavaPop", code: 999, userInfo: [NSLocalizedFailureReasonErrorKey : "An Unknow Error Occured"])
                        failure(genError)
                    }
                    return
            }
            //success
            success(items)
        }
    }
    
    func getPullRequests(forRepository: Repository, page: Int, resultsPerPage: Int, successBlock _success: @escaping ([PullRequest]) -> Void, failureBlock _failure: @escaping (Error) -> Void) {
        
        let success: ([PullRequest]) -> Void = { pullRequests in
            DispatchQueue.main.async { _success(pullRequests) }
        }
        let failure: (Error) -> Void = { error in
            DispatchQueue.main.async { _failure(error) }
        }
        
        guard let repositoryName = forRepository.name else { return }
        guard let repositoryOwnerName = forRepository.owner?.login else { return }
        
        let url = baseURL.appendingPathComponent("repos/\(repositoryOwnerName)/\(repositoryName)/pulls")
        
        let params: Parameters = [
            "state": "all",
            "page": page,
            "per_page": resultsPerPage,
            "client_id": clientID,
            "client_secret": clientSecret
        ]
        
        Alamofire.request(url, method: .get, parameters: params).responseData { (response) in
            //verify for errors
            guard let httpResponse = response.response, httpResponse.statusCode.isSuccessHTTPCode,
                let data = response.result.value else {
                    if let error = response.error {
                        failure(error)
                    } else {
                        //generic error
                        let genError = NSError(domain: "Github JavaPop", code: 999, userInfo: [NSLocalizedFailureReasonErrorKey : "An Unknow Error Occured"])
                        failure(genError)
                    }
                    return
            }
            
            guard let json = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) else {
                let genError = NSError(domain: "Github JavaPop", code: 888, userInfo: [NSLocalizedFailureReasonErrorKey : "Could not parse Json Response"])
                failure(genError)
                return
            }
            
            guard let pullRequests = Mapper<PullRequest>().mapArray(JSONObject: json) else {
                let genError = NSError(domain: "Github JavaPop", code: 888, userInfo: [NSLocalizedFailureReasonErrorKey : "Could not parse Json Response"])
                failure(genError)
                return
            }
            
            success(pullRequests)
        }
        
    }
    
}
