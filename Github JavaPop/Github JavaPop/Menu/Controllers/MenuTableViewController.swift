//
//  MenuTableViewController.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 16/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import UIKit
import MMDrawerController

class MenuTableViewController: UITableViewController {
 
    let mainStoryboard: String = "Main"
    let menuCellIdentifier: String = "MenuCell"
    
    var menuItems: [MenuItem]!
    var currentSelectedItem: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMenuItems()
    }
    
    internal func configureMenuItems() {
        let repositoriesItem = MenuItem(title: MenuItemTitles.repoTitle, storyboardIdentifier: StoryBoardIdentifiers.repoNavigation)
        let aboutItem = MenuItem(title: MenuItemTitles.aboutTitle, storyboardIdentifier: StoryBoardIdentifiers.aboutNavigation)
        menuItems = [repositoriesItem, aboutItem]
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: menuCellIdentifier, for: indexPath)
        cell.textLabel?.text = NSLocalizedString(menuItems[indexPath.row].title, comment: "")
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        didSelectMenuItem(itemIndex: indexPath.row)
    }
    
    //change page
    internal func didSelectMenuItem(itemIndex: Int) {
        if currentSelectedItem == itemIndex {
            mm_drawerController.closeDrawer(animated: true, completion: nil)
        } else {
            currentSelectedItem = itemIndex
            setCenterViewControllerWithIdentifer(identifier: menuItems[itemIndex].storyboardIdentifier)
        }
    }
    
    internal func setCenterViewControllerWithIdentifer(identifier: String) {
        let storyboard = UIStoryboard(name: mainStoryboard, bundle: Bundle.main)
        let centerController = storyboard.instantiateViewController(withIdentifier: identifier) as! UINavigationController
        mm_drawerController.centerViewController = centerController
        mm_drawerController.closeDrawer(animated: true, completion: nil)
        
    }

}

