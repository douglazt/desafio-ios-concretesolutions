//
//  MenuItem.swift
//  Github JavaPop
//
//  Created by Douglas da silva santos on 17/10/17.
//  Copyright © 2017 Douglas Santos. All rights reserved.
//

import Foundation

struct MenuItem {
    var title: String
    var storyboardIdentifier: String
}

struct StoryBoardIdentifiers {
    static let repoNavigation = "RepoNavigationViewController"
    static let aboutNavigation = "AboutNavigationViewController"
}

struct MenuItemTitles {
    static let repoTitle = "Repositories"
    static let aboutTitle = "About"
}
